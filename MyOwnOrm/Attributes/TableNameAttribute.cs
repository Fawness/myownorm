﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyOwnOrm
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TableNameAttribute : Attribute
    {

        public string name { get; set; }
        public TableNameAttribute(string tabName)
        {
            this.name = tabName;
        }
    }
}
