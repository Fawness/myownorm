﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute() {}
    }
}
