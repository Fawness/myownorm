﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    class Program
    {
        static void Main(string[] args)
        {
            Phones siemens = new Phones("5","panasonic","11");
            List<dynamic> telephones = new List<dynamic>();
            telephones.Add(siemens);

            Users user2 = new Users("11", "Daria", "Tolkien", telephones);

            var g = new Generator();
            // var getbyID = g.getById<Users>(1);
            // var getAll = g.getAll<Users>();
            // List<Users> allUsers = g.getAll<Users>();
            g.Insert<Users>(user2);
            g.DeleteById<Users>(9);
            //   g.UpdateItem<Users>(user2);

            Console.ReadKey();
        }
    }
}
