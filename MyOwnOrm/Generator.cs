﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Dynamic;
using System.Reflection;
using System.Collections;

namespace MyOwnOrm
{
    public class Generator
    {
        PrimaryKeyHelper pkHelper = new PrimaryKeyHelper();
        ForeignKeyHelper fkHelper = new ForeignKeyHelper();
        BuilderStringHelper stringhelper = new BuilderStringHelper();

        public Generator() { }

        public string tableAttrPresent(Type myType)
        {
            object[] attrs = myType.GetCustomAttributes(typeof(TableNameAttribute), true);

            if (attrs.Length > 0)
            {
                foreach (TableNameAttribute a in attrs)
                {
                    return a.name.ToString();
                }
            }
            return myType.Name.ToString();
        }

        public List<T> getAll<T>()
        {
            using (DBConnect connect = new DBConnect())
            {
                string query = "SELECT * From ";
                Type myType = typeof(T);
                List<T> result = new List<T>();
                query = query + tableAttrPresent(myType);
                SqlDataReader reader = connect.executeQuery(query);
                object[] totalProperties = myType.GetProperties();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        object pkValue = new object();
                        bool check = false;
                        object[] arrayOfParameters = new object[totalProperties.Length];
                        for (int j = 0; j < totalProperties.Length; j++)
                        {
                            object compareProperties = totalProperties[j];
                            foreach (PropertyInfo prop in totalProperties)
                            {
                                if (typeof(IList).IsAssignableFrom(prop.PropertyType) & compareProperties.Equals(prop))
                                {
                                    Type type = prop.PropertyType;
                                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                                    {
                                        Type itemType = type.GetGenericArguments()[0];
                                        var instance = OperationByFK(itemType, pkValue);
                                        arrayOfParameters[j] = instance;
                                    }
                                }
                                else
                                {
                                    if (compareProperties.Equals(prop))
                                    {
                                        string takeit = reader.GetValue(j).ToString();
                                        arrayOfParameters[j] = takeit;
                                        if (pkHelper.ifPkExist(prop) & check == false)
                                        {
                                            pkValue = takeit;
                                            check = true;
                                        }
                                    }
                                }
                            }
                        }
                        var newObj = (T)Activator.CreateInstance(typeof(T), arrayOfParameters);
                        result.Add(newObj);
                    }
                }
                return result;
            }
        }


        public object getById<T>(object ID)
        {
            using (DBConnect connect = new DBConnect())
            {
                string query = "SELECT * From ";
                Type myType = typeof(T);
                string primaryKey = pkHelper.getPrimaryKey(myType);

                query = query + tableAttrPresent(myType) + " Where " + primaryKey + "= '" + ID + "'";
                SqlDataReader reader = connect.executeQuery(query);
                var result = new object();
                object[] totalProperties = myType.GetProperties();

                if (reader.HasRows)
                {
                    object[] arrayOfParameters = new object[totalProperties.Length];
                    while (reader.Read())
                    {
                        for (int j = 0; j < totalProperties.Length; j++)
                        {
                            object compareProperties = totalProperties[j];
                            foreach (PropertyInfo prop in totalProperties)
                            {
                                if (typeof(IList).IsAssignableFrom(prop.PropertyType) & compareProperties.Equals(prop))
                                {
                                    Type type = prop.PropertyType;
                                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                                    {
                                        Type itemType = type.GetGenericArguments()[0];
                                        var instance = OperationByFK(itemType, ID);
                                        arrayOfParameters[j] = instance;
                                    }
                                }
                                else
                                {
                                    if (compareProperties.Equals(prop))
                                    {
                                        string takeit = reader.GetValue(j).ToString();
                                        arrayOfParameters[j] = takeit;
                                    }
                                }
                            }
                        }
                    }
                    var newObj = (T)Activator.CreateInstance(typeof(T), arrayOfParameters);
                    result = newObj;
                }
                return result;
            }
        }

        public void Insert<T>(object obj)
        {
            using (DBConnect connect = new DBConnect())
            {
                Type myType = typeof(T);
                object pkValue = pkHelper.getPrimaryKeyValue(myType, obj);
                object[] totalProperties = myType.GetProperties();
                bool checkIfExist = checkIfThatIdExist(myType, obj);
                string query;
                string constructor;
                SqlDataReader reader;

                if (!checkIfExist)
                {
                    foreach (PropertyInfo prop in totalProperties)
                    {
                        if (typeof(IList).IsAssignableFrom(prop.PropertyType))
                        {
                            Type type = prop.PropertyType;
                            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                Type itemType = type.GetGenericArguments()[0];
                                var listOfObjects = (IList)prop.GetValue(obj, null);

                                foreach (object objCol in listOfObjects)
                                {
                                    checkIfExist = checkIfThatIdExist(itemType, objCol);
                                    if (!checkIfExist)
                                    {
                                        query = "INSERT INTO " + tableAttrPresent(itemType) + " VALUES(";
                                        constructor = stringhelper.stringBuilderWithoutProperties(objCol, itemType);
                                        query = query + constructor + " );";
                                        reader = connect.executeQuery(query);
                                    }
                                }
                            }
                        }
                    }
                    query = "INSERT INTO " + tableAttrPresent(myType) + " VALUES(";
                    constructor = stringhelper.stringBuilderWithoutProperties(obj, myType);
                    query = query + constructor + " );";
                    reader = connect.executeQuery(query);
                }
            }
        }

        public void DeleteById<T>(object ID)
        {
            using (DBConnect connect = new DBConnect())
            {
                Type myType = typeof(T);
                object checkElement = getById<T>(ID);
                object[] totalProperties = myType.GetProperties();
                bool checkIfExist = checkElement is T;
                string query;
                SqlDataReader reader;

                if (checkIfExist)
                {
                    foreach (PropertyInfo prop in totalProperties)
                    {
                        if (typeof(IList).IsAssignableFrom(prop.PropertyType))
                        {
                            Type type = prop.PropertyType;
                            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                Type itemType = type.GetGenericArguments()[0];
                                query = "DELETE FROM " + tableAttrPresent(itemType) + " WHERE " + fkHelper.getForeignKey(itemType) + "='" + ID + "';";
                                reader = connect.executeQuery(query);
                            }
                        }
                    }
                    query = "DELETE FROM " + tableAttrPresent(myType) + " WHERE " + pkHelper.getPrimaryKey(myType) + " ='" + ID + "';";
                    connect.closeConnection();
                    reader = connect.executeQuery(query);
                }
            }
        }

        public void UpdateItem<T>(object myObj)
        {
            using (DBConnect connect = new DBConnect())
            {
                Type myType = typeof(T);
                bool checkIfExist = checkIfThatIdExist(myType, myObj);
                object pkValue = pkHelper.getPrimaryKeyValue(myType, myObj);
                object[] totalProperties = myType.GetProperties();
                string query;
                SqlDataReader reader;
                string pkProperty;
                string preSet;
                string primaryKey;

                if (checkIfExist)
                {
                    foreach (PropertyInfo prop in totalProperties)
                    {
                        if (typeof(IList).IsAssignableFrom(prop.PropertyType))
                        {
                            Type type = prop.PropertyType;
                            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                Type itemType = type.GetGenericArguments()[0];
                                var listOfObjects = (IList)prop.GetValue(myObj, null);

                                foreach (object objCol in listOfObjects)
                                {
                                    checkIfExist = checkIfThatIdExist(itemType, objCol);
                                    if (checkIfExist)
                                    {
                                        query = "UPDATE " + tableAttrPresent(itemType) + " SET ";
                                        pkProperty = pkHelper.getPrimaryKey(itemType);
                                        preSet = stringhelper.stringBuilderWithProperties(objCol, itemType, pkProperty);
                                        primaryKey = " WHERE " + pkProperty + " =" + pkHelper.getPrimaryKeyValue(itemType, objCol) + ";";
                                        query = query + preSet + primaryKey;
                                        reader = connect.executeQuery(query);
                                    }
                                }
                            }
                        }
                    }
                    query = "UPDATE " + tableAttrPresent(myType) + " SET ";
                    pkProperty = pkHelper.getPrimaryKey(myType);
                    preSet = stringhelper.stringBuilderWithProperties(myObj, myType, pkProperty);
                    primaryKey = " WHERE " + pkProperty + " =" + pkHelper.getPrimaryKeyValue(myType, myObj) + ";";
                    query = query + preSet + primaryKey;
                    reader = connect.executeQuery(query);
                }
            }
        }


        public List<dynamic> OperationByFK(Type type, object pkID)
        {
            using (DBConnect connect = new DBConnect())
            {
                object pk = pkID;
                string query = "SELECT * From ";
                Type myType = type;
                List<dynamic> result = new List<dynamic>();
                string fkName = fkHelper.getForeignKey(myType);

                query = query + tableAttrPresent(myType) + " WHERE " + fkName + " = " + pkID;
                SqlDataReader reader = connect.executeQuery(query);
                object[] totalFields = myType.GetProperties();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        object[] arrayOfParameters = new object[totalFields.Length];
                        for (int j = 0; j < totalFields.Length; j++)
                        {
                            string takeit = reader.GetValue(j).ToString();
                            arrayOfParameters[j] = takeit;
                        }
                        var newObj = Activator.CreateInstance(myType, arrayOfParameters);
                        result.Add(newObj);
                    }
                }
                return result;
            }
        }

        public bool checkIfThatIdExist(Type myType, object myObj)
        {
            using (DBConnect connect = new DBConnect())
            {
                string query = "SELECT * From ";
                string pkValue;
                if (myObj is int)
                {
                     pkValue = myObj.ToString();
                }
                else
                {
                     pkValue = pkHelper.getPrimaryKeyValue(myType, myObj);
                }
                string primaryKey = pkHelper.getPrimaryKey(myType);
                query = query + tableAttrPresent(myType) + " Where " + primaryKey + "= '" + pkValue + "'";
                SqlDataReader reader = connect.executeQuery(query);
                if (reader.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}