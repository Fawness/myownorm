﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    [TableName("Phones")]
    public class Phones
    {
        [PrimaryKey]
        public int id { get; set; }

        public string name { get; set; }

        [ForeignKey]
        public int userId { get; set; }

        public Phones() { }

        public Phones(string id, string nm)
        {
            this.id = Int32.Parse(id);
            this.name = nm;
        }

        public Phones(string id, string nm, string userID)
        {
            this.id = Int32.Parse(id);
            this.name = nm;
            this.userId = Int32.Parse(userID);
        }
        
    }


    [TableName("Users")]
    public class Users
    {
        [PrimaryKey]
        public int id { get; set; }

        public string name { get; set; }

        public string lastname { get; set; }

        [ForeignKey]
        public List<Phones> phones { get; set; }

        public Users()
        {  }
  
        public Users(string nm)
        {
            this.name = nm;
        }
        public Users(string ids, string nm, string lastname)
        {
            this.id = Int32.Parse(ids);
            this.name = nm;
            this.lastname = lastname;
        }

        public Users(string ids, string nm, string lastname, List<dynamic> listPh)
        {
            List<Phones> listPhones = listPh.Cast<Phones>().ToList();

            this.id = Int32.Parse(ids);
            this.name = nm;
            this.lastname = lastname;
            this.phones = listPhones;
        }
              
    }
}
