﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    public class PrimaryKeyHelper
    {
        public string getPrimaryKey(Type myType)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());
            string pkName = "";
            foreach (PropertyInfo property in properties)
            {
                object ifAttributeExist = property.GetCustomAttribute<PrimaryKeyAttribute>();
                if (ifAttributeExist != null)
                {
                    pkName = property.Name;
                    return pkName;
                }
            }
            return pkName;
        }
        public string getPrimaryKeyValue(Type myType, object obj)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());
            string pkValue = "";
            foreach (PropertyInfo property in properties)
            {
                object ifAttributeExist = property.GetCustomAttribute<PrimaryKeyAttribute>();
                if (ifAttributeExist != null)
                {
                    pkValue = property.GetValue(obj).ToString();
                    return pkValue;
                }
            }
            return pkValue;
        }

        public bool ifPkExist(PropertyInfo property)
        {
            object ifAttributeExist = property.GetCustomAttribute<PrimaryKeyAttribute>();
            if (ifAttributeExist != null)
            {
                return true;
            }
                return false;
        }
    }
}
