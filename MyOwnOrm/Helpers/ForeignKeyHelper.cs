﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    class ForeignKeyHelper
    {

        public string getForeignKey(Type myType)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());
            string fkName = "";
            foreach (PropertyInfo property in properties)
            {
                object ifAttributeExist = property.GetCustomAttribute<ForeignKeyAttribute>();
                if (ifAttributeExist != null)
                {
                    fkName = property.Name;
                    return fkName;
                }
            }
            return fkName;
        }
        public string getForeignKeyValue(Type myType, object obj)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());
            string fkValue = "";
            foreach (PropertyInfo property in properties)
            {
                object ifAttributeExist = property.GetCustomAttribute<ForeignKeyAttribute>();
                if (ifAttributeExist != null)
                {
                    fkValue = property.GetValue(obj).ToString();
                    return fkValue;
                }
            }
            return fkValue;
        }
    }
}
