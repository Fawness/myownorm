﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    public class BuilderStringHelper
    {
        public string stringBuilderWithProperties(object myObj, Type myType, string pkProperty)
        {
            string temporaryString = "";
            int count = 0;
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());
            int propertiesCount = propertiesCounter(properties);

            foreach (PropertyInfo property in properties)
            {
                object fieldValue = property.GetValue(myObj);
                if (pkProperty != property.Name)
                {
                    if (fieldValue is int)
                    {
                        temporaryString += property.Name + "=" + property.GetValue(myObj);
                    }
                    else if (typeof(IList).IsAssignableFrom(property.PropertyType))
                    {

                    }
                    else
                    {
                        temporaryString += property.Name + "='" + property.GetValue(myObj) + "'";
                    }

                    if (count < propertiesCount - 1)
                    {
                        temporaryString += ", ";
                    }
                }
                count++;
            }
            return temporaryString;
        }

        public string stringBuilderWithoutProperties(object myObj, Type myType)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(myType.GetProperties());

            int index = 0;
            string constructor = "";
            int propertiesCount = propertiesCounter(properties);

            foreach (PropertyInfo property in properties)
            {
                object fieldValue = property.GetValue(myObj);

                if (fieldValue is int)
                {
                    constructor += fieldValue.ToString();
                }
                else if (typeof(IList).IsAssignableFrom(property.PropertyType))
                {
                    
                }
                else
                {
                    constructor += "'" + fieldValue + "'";
                }


                if (index < propertiesCount - 1)
                {
                    constructor += ", ";
                }
                index++;
            }
            return constructor;
        }


        public int propertiesCounter(List<PropertyInfo> properties)
        {
            int index = properties.Count;
            foreach (PropertyInfo property in properties)
            {
                if (typeof(IList).IsAssignableFrom(property.PropertyType))
                {
                    index = index - 1;
                }
            }
            return index;
        }
    }
}
