USE [master]
GO
/****** Object:  Database [usersdb]    Script Date: 1/3/2017 1:24:24 PM ******/
CREATE DATABASE [usersdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'usersdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\usersdb.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'usersdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\usersdb_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [usersdb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [usersdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [usersdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [usersdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [usersdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [usersdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [usersdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [usersdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [usersdb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [usersdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [usersdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [usersdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [usersdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [usersdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [usersdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [usersdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [usersdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [usersdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [usersdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [usersdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [usersdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [usersdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [usersdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [usersdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [usersdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [usersdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [usersdb] SET  MULTI_USER 
GO
ALTER DATABASE [usersdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [usersdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [usersdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [usersdb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [usersdb]
GO
/****** Object:  Table [dbo].[Phones]    Script Date: 1/3/2017 1:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phones](
	[id] [nchar](10) NOT NULL,
	[name] [nchar](10) NULL,
	[userId] [nchar](10) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/3/2017 1:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [nchar](10) NOT NULL,
	[name] [nchar](10) NOT NULL,
	[lastname] [nchar](10) NULL
) ON [PRIMARY]

GO
INSERT [dbo].[Phones] ([id], [name], [userId]) VALUES (N'1         ', N'Nokia     ', N'1         ')
INSERT [dbo].[Phones] ([id], [name], [userId]) VALUES (N'2         ', N'Samsung   ', N'1         ')
INSERT [dbo].[Phones] ([id], [name], [userId]) VALUES (N'3         ', N'Toshiba   ', N'2         ')
INSERT [dbo].[Phones] ([id], [name], [userId]) VALUES (N'4         ', N'siemens   ', N'10        ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'8         ', N'Dima      ', N'Gladkov   ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'2         ', N'Tolya     ', N'Sokol     ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'3         ', N'Kirill    ', N'Kostwe    ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'9         ', N'Dima      ', N'Gladkov   ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'7         ', N'Alex      ', N'Kostwe    ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'5         ', N'Alex      ', N'Kostwe    ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'10        ', N'Mike      ', N'Tolkien   ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'1         ', N'Sasha     ', N'Lokos     ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'4         ', N'Natallia  ', N'Polovets  ')
INSERT [dbo].[Users] ([id], [name], [lastname]) VALUES (N'6         ', N'Misha     ', N'Failer    ')
USE [master]
GO
ALTER DATABASE [usersdb] SET  READ_WRITE 
GO
