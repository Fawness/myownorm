﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnOrm
{
    public class DBConnect : IDisposable
    {
        private SqlConnection connection;
        private string connectionPath = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public DBConnect()
        {
            this.connection = new SqlConnection(connectionPath);
        }

        public void openConnection()
        {
            this.connection.Open();
        }

        public void closeConnection()
        {
            this.connection.Close();
        }

        public SqlConnection getConnection()
        {
            return this.connection;
        }

        public bool isSqlConnectionStillOpen()
        {
            if (this.connection.State != ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public SqlDataReader executeQuery(string sqlExpression)
        {
            if(this.connection.State != ConnectionState.Open)
            {
                openConnection();
            }
               
          
                SqlCommand command = new SqlCommand(sqlExpression, this.connection);
                SqlDataReader reader = command.ExecuteReader();

                return reader;
        }

        public void Dispose()
        {
            bool ifOpen = isSqlConnectionStillOpen();
            if (ifOpen.Equals(true))
            {
                closeConnection();
            }
        }
    }
}
